Steps for running this code:---


Step 1 : Download code from this repository it contains two files csv_search.py and nyt_bestsellers.csv
         Both the files need to be in the same folder i.e. at the same location.
         
         
Step 2 : Now open csv_search.py and run the code.


               - Enter your Choice (either 1 or 2) and press Enter key
               
               - Upon entering choice 1 enter the exact title given in CSV file and press Enter key
               
               - If your choice is 2 enter the correct genre given in CSV file and press Enter key
                
               
                